BACKUP DATABASE [shop] TO  DISK = N'/var/opt/mssql/data/shop.bak' WITH  COPY_ONLY, 
NOFORMAT, NOINIT,  NAME = N'master-Full Database Backup', SKIP, NOREWIND, NOUNLOAD,  STATS = 10
GO


RESTORE DATABASE shop
FROM DISK = N'/var/opt/mssql/data/shop.bak'
WITH RECOVERY, REPLACE